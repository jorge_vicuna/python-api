from src.services.elevate_two_numbers import elevate_two_numbers


def test_elevate_numbers():
  factor_1, factor_2 = 2, 3
  res = elevate_two_numbers(factor_1, factor_2)
  assert res == 8
