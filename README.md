# Ejemplo de Python API Web con GitLab CI/CD

[![pipeline status](https://gitlab.com/taller-gitlab-ci/python-api/badges/master/pipeline.svg)](https://gitlab.com/taller-gitlab-ci/python-api/-/commits/master)
[![coverage report](https://gitlab.com/taller-gitlab-ci/python-api/badges/master/coverage.svg)](https://gitlab.com/taller-gitlab-ci/python-api/-/commits/master)

Este proyecto es una API web de ejemplo hecho en el lenguaje Python.

## Cómo correr el proyecto
El proyecto utiliza Python 3.8 y la herramienta para crear un entorno virtual de Python [pipenv](https://pipenv-es.readthedocs.io/es/latest/).

1. Instala pipenv en tu ambiente de desarrollo. `pip` es el gestor de paquetes de Python por defecto, similar a `npm` de Javascript, `maven` de Java, `composer` de PHP, etc.

```bash
pip install pipenv
```

2. Instala las dependencias con `pipenv`

```bash
pipenv install
```

4. Instalar uvicorn

```
pip install uvicorn
```

5. Corre el proyecto con `pipenv`

```
pipenv run uvicorn main:app --reload
```

6. Abre la siguiente url en el navegador.

```
http://127.0.0.1:8000/docs
```

![api web](imgs/fast-api-python-example.png "documentación de la API Web")


## Cómo correr las pruebas unitarias
Para correr las pruebas unitarias que se encuentran en la carpeta `test`, ejecutar el siguiente comando:

## instalar esto

pip install pytest-cov 

## Luego correr esto
```
pipenv run pytest --cov=src test/
```

Los tests de esta carpeta testean por el momento los servicios implementados en la carpeta `services`


## pipenv run coverage xml

## Para desplegar luego en Horuko
uvicorn = {extras = ["standard"], version = "*"}// añadir en Pipfile
crear archivo:
Procfile
web: uvicorn main:app --host=0.0.0.0 --port=${PORT:-5000}
web: uvicorn src.main:app --host=0.0.0.0 --port=${PORT:-5000}


git remote -v // se coloca en terminal para ver los entornos
EJECUTAR Un pipenv install para actualizar un archivoh